(function( $ ) {
    
  var callink = 0;
  var methods = {  	
    init : function( options ) { 
        var $this = $(this);
        if($this.prop("tagName") !== 'INPUT' && $this.attr('type') !== 'file') return false;    
        callink++;

      var settings = $.extend({
		'cropSize' : [260, 260],
		'cropHolder' : 390,
		'debug' : false,
        'settings' : false,
        'lang' : 'ru',        
        'requestUrl' : ''} , options),
		holderW,
		holderH,
		cropSizeW,
		cropSizeH;		
		

    
    if(settings.cropHolder instanceof Array){
    	holderW = settings.cropHolder[0];
    	holderH = settings.cropHolder[1];
    }
    else if(settings.cropHolder > 0){
    	holderW = holderH = settings.cropHolder;
    }   

	if(settings.cropSize instanceof Array){
    	cropSizeW = settings.cropSize[0];
    	cropSizeH = settings.cropSize[1];
    }
    else if(settings.cropSize > 0){
    	cropSizeW = cropSizeH = settings.cropSize;
    }
    if(settings.settings && typeof(Storage) !== "undefined"){
        cropSizeW = localStorage.getItem("СropSizeW") ? localStorage.getItem("СropSizeW") : cropSizeW;
        cropSizeH = localStorage.getItem("СropSizeH") ? localStorage.getItem("СropSizeH") : cropSizeH;
        holderW = localStorage.getItem("holderW") ? localStorage.getItem("holderW") : holderW;
        holderH = localStorage.getItem("holderH") ? localStorage.getItem("holderH") : holderH;
    }
    var inputID = $this.attr('id'),
        params ={
            'cropSizeW'  : cropSizeW,
            'cropSizeH'  : cropSizeH,
            'holderW'    : holderW,
            'holderH'    : holderH,
            'kMax'       : (Math.max(cropSizeW,cropSizeH) / Math.min(cropSizeW,cropSizeH)),
            'cropK'      : cropSizeW/cropSizeH,
            'cropMax'    : Math.max(cropSizeW, cropSizeH),
            'requestUrl' : settings.requestUrl,
            'debug'      : settings.debug
        };
        
    $this.data('params', params);
    $.getJSON('TCrop_' + settings.lang ).success(function(data){
    var lang = data;
   
    if($this.addClass('TCropInput').parent().prop("tagName") !== 'LABEL'){
        if(!$this.attr('id'))
            $this.attr('id', 'TCropInput' + callink);
        $this.wrap($('<label>', {"class" : "TCropLabel", "for" : $this.attr('id')})
                .width(cropSizeW)
                .height(cropSizeH) 
                )        
        .before($('<span>',{"text" : lang.TCropLabel}))
        .before($('<div>') 
                .width(cropSizeW)
                .height(cropSizeH)
                .css({'background-image' : 'url(' + $this.data('img') +')'})
                );

    }
    var TCropOverX1  = $('<div/>',{
	    	class : 'TCropOverX',
	    	height : (holderH - cropSizeH)/2 
	    }),
    	TCropOverX2 = TCropOverX1.clone(),
    	TCropOverY1 = $('<div/>',{
	    	class : 'TCropOverY',
	    	width : (holderW - cropSizeW)/2 ,
	    	height: cropSizeH 
	    }),
	    TCropOverY2 = TCropOverY1.clone(),
	    TCropVisual = $('<div/>',{
	    	class : 'TCropVisual TCropStandby',
	    	width : cropSizeW,
	    	height: cropSizeH
	    }),
    	TCropOverlay = $('<div/>', {
 			class : 'TCropOverlay', 			
 			append : TCropOverX1
 				.add(TCropOverY1)
 				.add(TCropVisual)
 				.add(TCropOverY2)
 				.add(TCropOverX2)

		}),
		TCropSlider  = $('<div/>', {class : 'TCropSlider'}),    	
    	TCropContain = $('<div/>', {class : 'TCropContain'}),
    	TCropHelper  = $('<div/>', {class: 'TCropHelper'}),
    	TCropImg     = $('<img/>', {class: 'TCropImg', src: (settings.debug ? 'square.png' : '') }).hide(),    	
    	TCropHolder  = $('<div/>',{ 
	 		class : 'TCropHolder',
	 		width : holderW,
 			height : holderH,
	 		append : TCropOverlay
	 			.add(TCropContain
	 				.append(TCropHelper)
	 				.append(TCropImg)
	 			)	 			
	 		}),
        TCropSend  = $('<input/>',{class : 'TCropSend', type:'image', src:'', 'onclick':'$("#' + inputID + '").TCrop("send");','value' : lang.TCropSend}).button(),
        TCropWrap  = $('<div/>',{class : 'TCropWrap', append : TCropHolder.add(TCropSlider).add(TCropSend) }).appendTo('body');




        if(settings.debug){
            var testPanel=$('<div>', {'class' : 'TCropTestpanel', 
                append : $('<input/>',{type:'text',id:'testlink','placeholder':'use img link'})
            .add($('<input/>',{type:'button','onclick':'$(".TCropImg").attr("src",$("#testlink").val());','value':'OK'}).button())
            .add($('<hr/>',{'style':'margin:10px 0;'}))
            .add($('<input/>',{type:'button', 'class': 'TCroptestset','onclick':'$(".TCropImg").attr("src",$(this).val()+".png");','value':'horizontal'}))
            .add($('<input/>',{type:'button', 'class': 'TCroptestset','onclick':'$(".TCropImg").attr("src",$(this).val()+".png");','value':'vertical'}))
            .add($('<input/>',{type:'button', 'class': 'TCroptestset','onclick':'$(".TCropImg").attr("src",$(this).val()+".png");','value':'square'}))
        });
            testPanel.find('.TCroptestset').button();
            TCropWrap.prepend(testPanel);
            
        }
        if(settings.settings){
                var СropSizeW = params.cropSizeW,
                    СropSizeH = params.cropSizeH,
                    HolderW = params.holderW,
                    HolderH = params.holderH;  
            if(typeof(Storage) !== "undefined") {
                СropSizeW = localStorage.getItem("СropSizeW") ? localStorage.getItem("СropSizeW") : params.cropSizeW;
                СropSizeH = localStorage.getItem("СropSizeH") ? localStorage.getItem("СropSizeH") : params.cropSizeH;
                HolderW = localStorage.getItem("holderW") ? localStorage.getItem("holderW") : params.holderW;
                HolderH = localStorage.getItem("holderH") ? localStorage.getItem("holderH") : params.holderH;                
            }             
            var settingsWrap = $('<div>', {'class' : 'TCropSettingsWrap', text : lang.TCropSettingsWrap,
                append : $('<div>', {'class':'TCropScrop', text : lang.TCropScrop,
                    append : $('<input>', {type : 'text', 'class' : 'TCropScropW', value : СropSizeW})
                        .add($('<input>', {type : 'text', 'class' : 'TCropScropH', value : СropSizeH}))
                })              
                    .add( $('<div>', {'class':'TCropSholder', text : lang.TCropSholder,
                                            append : $('<input>', {type : 'text', 'class' : 'TCropSholderW', value : HolderW})
                            .add($('<input>', {type : 'text', 'class' : 'TCropSholderH', value : HolderH}))
                    }))
                    .add($('<input>', {type : 'button', 'class' : 'TCropSapply', value : lang.TCropSapply})
                        .button()
                        .on('click', function(){
                            var newСropSizeW = settingsWrap.find('.TCropScropW').val(),
                                newСropSizeH = settingsWrap.find('.TCropScropH').val(),
                                newholderW = settingsWrap.find('.TCropSholderW').val(),
                                newholderH = settingsWrap.find('.TCropSholderH').val();
                            localStorage.setItem("СropSizeW", newСropSizeW);         
                            localStorage.setItem("СropSizeH", newСropSizeH);         
                            localStorage.setItem("holderW", newholderW);         
                            localStorage.setItem("holderH", newholderH);         
                            $this.TCrop('cropResize', newСropSizeW, newСropSizeH)
                                 .TCrop('holderResize', newholderW, newholderH);                          
                          
                        })
                    )
                });
            TCropWrap.prepend(settingsWrap);

        }
    	$this.data('contain',{
            'wrap':TCropWrap,
    		'it':TCropContain,
    		'helper':TCropHelper,
    		'img':TCropImg
    	});
    	$this.on('change', function(){$this.TCrop('change')});		
    	TCropHelper.draggable({
            addClasses: false,
            containment: TCropContain,
            drag: function () {
                TCropImg.css(TCropHelper.position());
            },
            stop: function () {
                TCropImg.css(TCropHelper.position());
            }
        });

        TCropSlider.slider({
            value: 100,
            min: 100,
            max: 300,
            step: 1,
            slide: function (event, ui) {
				$this.TCrop('imgResize', ui.value);
            },
            stop: function () {
           		$this.TCrop('updateHelper').TCrop('updateContain');
            }
        });
    	TCropImg.load(function(){
            TCropVisual.removeClass('TCropStandby');
            $(this).show();
            var imgNW=$(this).get(0).naturalWidth,
                imgNH=$(this).get(0).naturalHeight;
                var imgk = 1;
            if(imgNW > imgNH)
                imgK = imgNW / imgNH;
            else
                imgK = imgNH / imgNW;
    		$this.data('params').imgK = imgK;
            $this.data('params').naturalWidth = imgNW;
            $this.data('params').naturalHeight = imgNH;
	    	$this.TCrop('imgResize').TCrop('updateHelper').TCrop('updateContain');

            TCropSlider.slider({value: 100});

		});
		var dialog_param = {
            autoOpen: false,
            modal: true,
            resizable: false,
            position: 'center',
            draggable: false,
            width: 'auto',
            title: '',
            closeText: '',
        };
        TCropWrap.dialog(dialog_param);
        })//end getJson lang success;
        .error(function(data){console.log('Lang file not found')});
    },
    change : function() {
        var $this=$(this),
            contain = $this.data('contain'),
            input = this[0];
        if(input.files && input.files[0]){
         
        var reader = new FileReader();

        
        reader.onload = function (e) {
            contain.img.attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);

        
        }
        contain.wrap.dialog( "open" );
    },
    imgResize : function(prc) {
    	var $this=$(this),
    		contain = $this.data('contain'),
    		img=contain.img,
    		helper=contain.helper,
    		params=$this.data('params'),
    		cropK = params.cropK,
    		imgK = params.imgK, 
            cropMax= params.cropMax,
            imgSub;   		

		if(!img.attr('src')) return false;

        img.height('auto');
        img.width('auto');

		if(!prc){	

                if(params.naturalWidth > params.naturalHeight){
                    if(params.cropSizeH*imgK < params.cropSizeW)
                        img.width(params.cropSizeW);
                    else
                        img.height(params.cropSizeH);
                }else{
                    if(params.cropSizeW*imgK < params.cropSizeH)
                        img.height(params.cropSizeH);
                    else
                        img.width(params.cropSizeW);
                    
                }

			var imgNormalW = img.width(),
				imgNormalH = img.height();
			params.imgNormalW=imgNormalW;
				  
			img.css({'left':(imgNormalW-params.cropSizeW)/2, 'top':(imgNormalH-params.cropSizeH)/2, 'display':'block'});			
		}
		else{

			var newImgW = params.imgNormalW / 100 * prc,													
				imgPosition=img.position();
			img.height('auto');
			img.width(newImgW);

			var	newImgH=img.height(),
				left = imgPosition.left + (newImgW  - params.imgOldW)/2,
				top = imgPosition.top +   (newImgH  - params.imgOldH)/2;				
					
			$this.TCrop('updateContain');

			var maxLeft=(params.oldContainW-params.cropSizeW)/2,
				maxTop=(params.oldContainH-params.cropSizeH)/2;
			left= left <= 0 ? 0 : (left >= maxLeft ? maxLeft : left);
			top = top  <= 0 ? 0  : (top >= maxTop ? maxTop : top); 	
			img.css({'left':left, 'top':top});
		}		

		params.imgOldW=img.width();
		params.imgOldH=img.height();
      
		return $this;
    },
    
    updateHelper : function(){
		var $this=$(this),
			contain = $this.data('contain'),
			img=contain.img,
			helper=contain.helper;
       
    	helper.css({ 'width':img.width(), 'height':img.height(), 'left': img.css('left'), 'top':img.css('top')});

    	return $this;
    },

    updateContain : function() {
     	var $this=$(this),
     		contain = $this.data('contain'),
     		params = $this.data('params');
     		containW=contain.img.width() * 2 - params.cropSizeW,
     		containH=contain.img.height() * 2 - params.cropSizeH;
     				contain.it.css({
			'width': Math.abs(containW),
            'height':Math.abs(containH),
            'marginLeft': -containW/2 ,
            'marginTop': -containH/2 
		});
		params.oldContainW=Math.abs(containW);
		params.oldContainH=Math.abs(containH);
		return $this;
    },
    cropResize : function(width, height){
        var $this = $(this),
            wrap = $this.data('contain').wrap,
            holder =wrap.find('.TCropHolder');
            wrap.find('.TCropVisual').css({'width' : width, 'height' : height});
            wrap.find('.TCropOverX').css({'height' : (holder.height() - height)/2});
            wrap.find('.TCropOverY').css({'height' : height, 'width' : (holder.width() - width)/2});
            $this.data('params').cropSizeW = width;
            $this.data('params').cropSizeH = height;
            $this.TCrop('imgResize').TCrop('updateHelper').TCrop('updateContain');
            wrap.find('.TCropSlider').slider({value: 100});
        return $this
    },
    holderResize : function(width, height){
        var $this  = $(this),
            holder = $this.data('contain').wrap.find('.TCropHolder'); 
        holder.width(width);
        holder.height(height);
        $this.data('params').holderW = width;
        $this.data('params').holderH = height;
        $this.TCrop('cropResize', $this.data('params').cropSizeW, $this.data('params').cropSizeH );
        return $this
    },
    send : function(){
        var $this=$(this),
            contain = $this.data().contain,
            params  = $this.data().params,
            imgResW = contain.img.width(),
            imgResH = contain.img.height(),
            imgW    = contain.img.get(0).naturalWidth,
            imgH    = contain.img.get(0).naturalHeight,            
            fd      = new FormData(),
            cropX   = (imgResW - params.cropSizeW - contain.img.position().left),
            cropY   = (imgResH - params.cropSizeH - contain.img.position().top),
            button  = contain.wrap.find('.TCropSend');
        fd.append('imgFile' , this[0].files[0]);
        fd.append('cropW' , params.cropSizeW);
        fd.append('cropH' , params.cropSizeH);
        fd.append('imgW'  , imgW);
        fd.append('imgH'  , imgH);
        fd.append('imgResW' , imgResW);
        fd.append('imgResH' , imgResH);
        fd.append('cropX' , cropX < 0 ? 0 : cropX);
        fd.append('cropY' , cropY < 0 ? 0 : cropY);  
        button.attr('src', 'load_b.gif').prop('disabled', true);
        $.ajax({
                url: params.requestUrl,
                data: fd,
                cache: false,
                contentType: false,
                dataType: 'html',
                processData: false,
                type: 'POST',
                success: function (data) {
                    var reUrl = '?' + Math.floor(Math.random(1, 100) * 1000);
                      $this.val('');
                      $this.parent().find('div').css('background-image', 'url(' + data + reUrl + ')');
                        
                  
                      button.attr('src', '').prop('disabled', false);
                      contain.wrap.dialog("close");
                }
            })
            .fail(function () {
                button.attr('src', '').prop('disabled', false);

            });
         
    }

  };
$.fn.TCrop = function( method ) {	
	if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 )); 
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method "' +  method + '" not found in jQuery.TCrop' );
    } 
    

  };

 
})(jQuery);
