<?
class Image{
	protected $filepath;
	protected $url;
	protected $cropImage;
	protected $holst;
	protected $imageType;
	protected $imageOriginalWidth;
	protected $imageOriginalHeight;
	protected $width;
	protected $height;
	function __construct($inputName = NULL){
		if(!$inputName || empty($_FILES[ $inputName ]))
			return;
		$this->setImage($this->upload($inputName));
		if(!$this->getImage())
			throw new Exception('Image upload error!'); 
		$this->genImageProp();
		$funcCreate = 'imagecreatefrom' . $this->getType();
		$this->holst = 	$funcCreate($this->filepath);
	}
	function crop($width, $height, $x, $y, $imageWidth=NULL, $imageHeigth=NULL) {
		if (!$imageWidth||!$imageHeigth) {
			$imageWidth=$this->imageOriginalWidth;
			$imageHeigth=$this->imageOriginalHeight;
		}else{
			$this->resize($imageWidth, $imageHeigth);
		}
		$new_holst = imagecreatetruecolor($width, $height);
		imagecopyresampled($new_holst, $this->holst, 0, 0, $x, $y, $width, $height, $width, $height);
		$this->holst = $new_holst;
		return $this;
	}
	function resize($width,$height){
		$new_image = imagecreatetruecolor($width, $height);
		imagecopyresampled($new_image, $this->holst, 0, 0, 0, 0, $width, $height, $this->width, $this->height);
		$this->width = $width;
		$this->height = $height;
		$this->holst = $new_image;
	}
	function upload($inputName, $dir = "imgout"){
		if(empty($_FILES[ $inputName ]))
			return false;
		$uploaddir = __DIR__ . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR;
		$imageFileProps = $this->getImageProp($_FILES[ $inputName ]['tmp_name']);	
		$filename = md5(time()) . "." . $imageFileProps['type'];
		while (file_exists($filename)){
			$filename = md5(time()) . "." .  $imageFileProps['type'];
		}		
		$uploadfile = $uploaddir . $filename;
		if( move_uploaded_file($_FILES[ $inputName ]['tmp_name'], $uploadfile) ){
			$this->url = '/' . $dir . '/' . $filename;
			return $uploadfile;	
		} else{
			return false;
		}

	}
	function save(){
		$funcImage = "image" . $this->getType();		
		$funcImage($this->holst, $this->filepath);		
		return $this;
	}
	function setImage($imagePath){
		$this->filepath = $imagePath;
	}
	function getImage(){
		return $this->filepath;
	}
	function getUrl(){
		return $this->url;
	}
	function getHolst(){
		if (!$this->holst){
			$func = 'imagecreatefrom' . $this->getType(); 
			$this->holst = $func($this->filepath);
		}
		return $this->holst;
	}
	function getType(){
		return $this->imageType;
	}
	function getImageProp($filepath = NULL){
		if(!$filepath){
			$filepath = $this->filepath;
		}
		list($w_i, $h_i, $type) = getimagesize($filepath); 
		$types = array("", "gif", "jpeg", "png");
		return array('w_i'=>$w_i, 'h_i'=>$h_i, 'type'=>$types[$type]);
	}
	function genImageProp(){
		$props = $this->getImageProp();
		$this->imageType = $props['type'];
		$this->height = $this->imageOriginalHeight = $props['h_i'];
		$this->width = $this->imageOriginalWidth = $props['w_i'];
		return $props;
	}
}